/*
 * rcNgMovable - very simple angular directive to move/drag element with HammerJS
 * http://github.com/jean-rakotozafy/rcNgMovable
 * (c) 2014 MIT License, https://www.m-ite.com
 */

(function (window, angular) {
    'use strict';

    var module = angular.module('rcNgMovable');

    module
        .constant('hammerConfigMovable', {
            transformMinScale: 1,
            transformMinRotation: 0,
            dragBlockHorizontal: true,
            dragBlockVertical: true,
            dragMinDistance: 0
        })
        .directive('rcNgMovable', ['MovableHammer', 'hammerConfigMovable', 'rcTransformService', '$M', '$log', function (Hammer, hammerConfigMovable, rcTransformService, $M, $log) {
            return{
                restrict: 'A',
                link: function ($scope, $element, $attrs) {

                    var posX = 0, posY = 0,
                        lastPosX = 0, lastPosY = 0,
                        domElement = $element[0],
                        oldZIndex;

                    var rcNgMovableLimit = ($attrs.rcNgMovableLimit === "true");

                    var hammerObj = new Hammer(domElement, hammerConfigMovable);

                    hammerObj.on('touch drag dragend', function (ev) {

                        ev.preventDefault();

                        if (ev.currentTarget !== ev.target)
                            return false;

                        var deltaX = ev.gesture.deltaX;
                        var deltaY = ev.gesture.deltaY;

                        switch (ev.type) {
                            case 'touch':
                                //$log.log('rcMovable touch');
                                oldZIndex = domElement.style.zIndex;
                                break;

                            case 'drag':
                                //$log.log('rcMovable move');
                                posX = deltaX + lastPosX;
                                posY = deltaY + lastPosY;
                                domElement.style.zIndex = 1000;
                                break;

                            case 'dragend':
                                //$log.log('rcMovable dragend');
                                lastPosX = posX;
                                lastPosY = posY;
                                domElement.style.zIndex = oldZIndex;

                                break;
                        }

                        if (ev.type === 'touch' || ev.type === 'dragend')
                            return;

                        var rect = domElement.getBoundingClientRect();
                        var w = rect.left + rect.width;
                        var h = rect.top + rect.height;


                        var parentRect = domElement.parentElement.getBoundingClientRect();
                        var pw = parentRect.left + parentRect.width;
                        var ph = parentRect.top + parentRect.height;

                        var biggerW = rect.width > parentRect.width;
                        var biggerH = rect.height > parentRect.height;

                        var deltaLeft = rect.left - parentRect.left;
                        var deltaTop = rect.top - parentRect.top;
                        var deltaRight = pw - w;
                        var deltaBottom = ph - h;

                        var mOld = rcTransformService.getCurrentMatrixTransform(domElement);
                        var decomposedMatrix = rcTransformService.decomposeMatrix(mOld);

                        var getLimit = false;

                        if (rcNgMovableLimit) {
                            //$log.log('rcMovable limit');
                            // Cas dans 1er exemple
                            if (deltaLeft <= 0 && deltaX < 0 && !biggerW) {
                                posX = mOld.e;
                                getLimit = true;
                            }

                            if (deltaRight <= 0 && deltaX > 0 && !biggerW) {
                                posX = mOld.e;
                                getLimit = true;
                            }

                            if (deltaTop < 0 && deltaY < 0 && !biggerH) {
                                posY = mOld.f;
                                getLimit = true;
                            }

                            if (deltaBottom < 0 && deltaY > 0 && !biggerH) {
                                posY = mOld.f;
                                getLimit = true;
                            }

                            // Cas dans 2ème exemple
                            if (deltaRight > 0 && deltaX < 0 && biggerW) {
                                posX = mOld.e;
                                getLimit = true;
                            }

                            if (deltaLeft > 0 && deltaX > 0 && biggerW) {
                                posX = mOld.e;
                                getLimit = true;
                            }

                            if (deltaTop > 0 && deltaY > 0 && biggerH) {
                                posY = mOld.f;
                                getLimit = true;
                            }

                            if (deltaBottom > 0 && deltaY < 0 && biggerH) {
                                posY = mOld.f;
                                getLimit = true;
                            }
                        }

                        var mNew = rcTransformService.buildMatrixFromParameters(posX, posY, decomposedMatrix.scaleX , decomposedMatrix.rotation);

                        /*var m = $M([
                         [mOld.a, mOld.c, 0],
                         [mOld.b, mOld.d, 0],
                         [0, 0, 1]
                         ]);
                         var matrix = m.x(mNew);*/

                        var matrixCss = rcTransformService.buildCssMatrixFromM(mNew);

                        rcTransformService.setElTransformStyle(domElement, matrixCss);

                    });
                }
            };
        }
        ]);

})(window, window.angular);
